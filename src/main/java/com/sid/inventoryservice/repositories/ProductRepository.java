package com.sid.inventoryservice.repositories;

import com.sid.inventoryservice.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {

    Page<Product> findAll(final Pageable pageable);
    Page<Product> findAllByDesignation(final String designation, final Pageable pageable);

    Optional<Product> getProductById(final UUID productId);
    Optional<Product> getProductByProductCode(final String productCode);
}
