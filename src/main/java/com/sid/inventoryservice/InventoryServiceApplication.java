package com.sid.inventoryservice;

import com.sid.inventoryservice.domain.Product;
import com.sid.inventoryservice.repositories.ProductRepository;
import com.sid.inventoryservice.web.dto.ProductDto;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.List;

@SpringBootApplication
public class InventoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(final ProductRepository productRepository){
		return args -> {
			productRepository.saveAllAndFlush(List.of(
					Product.builder()
							.designation("Hinekend")
							.quantity(50)
							.unitprice(BigDecimal.valueOf(1500))
							.build(),
					Product.builder()
							.designation("Coca-Cola")
							.quantity(20)
							.unitprice(BigDecimal.valueOf(1000))
							.build(),
					Product.builder()
							.designation("Tuborc")
							.quantity(15)
							.unitprice(BigDecimal.valueOf(700))
							.build()
			));

			productRepository.findAll().forEach(System.out::println);
		};
	}
}
