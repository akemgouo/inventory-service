package com.sid.inventoryservice.web.mappers;

import com.sid.inventoryservice.domain.Product;
import com.sid.inventoryservice.web.dto.ProductDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

@Mapper(uses = DateMapper.class)
@DecoratedWith(ProductMapperDecorator.class)
public interface ProductMapper {

    ProductDto convertEntityToDto(final Product product);

    Product convertDtoToEntity(final ProductDto dto);
}
