package com.sid.inventoryservice.web.mappers;

import com.sid.inventoryservice.domain.Product;
import com.sid.inventoryservice.web.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class ProductMapperDecorator implements ProductMapper {

    private ProductMapper productMapper;

    @Autowired
    public void setProductMapper(final ProductMapper productMapper){
        this.productMapper = productMapper;
    }
    @Override
    public ProductDto convertEntityToDto(final Product product) {
        return productMapper.convertEntityToDto(product);
    }

    @Override
    public Product convertDtoToEntity(final ProductDto dto) {
        return productMapper.convertDtoToEntity(dto);
    }
}
