package com.sid.inventoryservice.web.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;

public class ProductDtoPagedList extends PageImpl<ProductDto> implements Serializable {
    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public ProductDtoPagedList(@JsonProperty("content") List<ProductDto> content,
                               @JsonProperty("number") int number,
                               @JsonProperty("size") int size,
                               @JsonProperty("totalElements") Long totalElements,
                               @JsonProperty("pageable") JsonNode pageable,
                               @JsonProperty("last") boolean last,
                               @JsonProperty("totalPages") int totalPages,
                               @JsonProperty("sort") JsonNode sort,
                               @JsonProperty("first") boolean first,
                               @JsonProperty("numberOfElements") int numberOfElements) {
        super(content, PageRequest.of(number,size), totalElements);
    }

    public ProductDtoPagedList(List<ProductDto> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public ProductDtoPagedList(List<ProductDto> content) {
        super(content);
    }
}
