package com.sid.inventoryservice.web.controller.api;

import com.sid.inventoryservice.web.dto.ProductDto;
import com.sid.inventoryservice.web.dto.ProductDtoPagedList;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;

import static com.sid.inventoryservice.web.utils.Constants.*;

@Tag(name = "Product Api", description = "Product REST API")
@Api(value = APP_ROOT, produces = "http, https", protocols = MediaType.APPLICATION_JSON_VALUE, tags = "Product Api")
public interface ProductApi {

    @PostMapping(value = CREATE_PRODUCT_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ProductDto> save (@Validated @RequestBody final ProductDto productDto);

    /*@GetMapping(value = FIND_ALL_PRODUCTS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ProductDtoPagedList> listAllProducts(@RequestParam(value = "pageNumber", required = false) final Integer pageNumber,
                                                        @RequestParam(value = "pageSize", required = false) final Integer pageSize,
                                                        @RequestParam(value = "designation", required = false) final String designation);*/

    @GetMapping(value = FIND_PRODUCT_BY_ID_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ProductDto> getProductById(@PathVariable("productId") final UUID productId);

    @GetMapping(value = FIND_ALL_PRODUCTS_ENDPOINT, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<ProductDto>> findAll();
}
