package com.sid.inventoryservice.web.controller;

import com.sid.inventoryservice.services.ProductService;
import com.sid.inventoryservice.web.controller.api.ProductApi;
import com.sid.inventoryservice.web.dto.ProductDto;
import com.sid.inventoryservice.web.dto.ProductDtoPagedList;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ProductRestController implements ProductApi {

    @Value("${DEFAULT_PAGE_NUMBER}")
    private String DEFAULT_PAGE_NUMBER;

    @Value("${DEFAULT_PAGE_SIZE}")
    private String DEFAULT_PAGE_SIZE;

    private final ProductService productService;

    @Override
    public ResponseEntity<ProductDto> save(final ProductDto productDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.save(productDto));

    }

    /*@Override
    public ResponseEntity<ProductDtoPagedList> listAllProducts(Integer pageNumber, Integer pageSize, final String designation) {

        if (pageNumber == null || pageNumber < 0){
            pageNumber = Integer.valueOf(DEFAULT_PAGE_NUMBER);
        }

        if (pageSize == null || pageSize < 1) {
            pageSize = Integer.valueOf(DEFAULT_PAGE_SIZE);
        }

        ProductDtoPagedList productDtoPagedList = productService.listAllProduct(designation, PageRequest.of(pageNumber,pageSize));

        return ResponseEntity.status(HttpStatus.OK).body(productDtoPagedList);
    }*/

    @Override
    public ResponseEntity<ProductDto> getProductById(final UUID productId) {
        return ResponseEntity.status(HttpStatus.OK).body(productService.getProductById(productId));
    }

    @Override
    public ResponseEntity<List<ProductDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK).body(productService.findAll());
    }
}
