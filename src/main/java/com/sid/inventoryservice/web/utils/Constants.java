package com.sid.inventoryservice.web.utils;

public interface Constants {

    final static String APP_ROOT = "api/v1/product";

    final static String PRODUCT_ENDPOINT = APP_ROOT ;
    final static String CREATE_PRODUCT_ENDPOINT = PRODUCT_ENDPOINT + "/create";
    final static String FIND_PRODUCT_BY_ID_ENDPOINT = PRODUCT_ENDPOINT + "/{productId}";
    final static String FIND_ALL_PRODUCTS_ENDPOINT = PRODUCT_ENDPOINT + "/all";
}
