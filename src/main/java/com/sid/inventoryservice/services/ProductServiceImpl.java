package com.sid.inventoryservice.services;

import com.sid.inventoryservice.domain.Product;
import com.sid.inventoryservice.repositories.ProductRepository;
import com.sid.inventoryservice.web.dto.ProductDto;
import com.sid.inventoryservice.web.dto.ProductDtoPagedList;
import com.sid.inventoryservice.web.mappers.ProductMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    @Override
    public ProductDtoPagedList listAllProduct(final String designation, PageRequest pageRequest) {
        System.out.println("I was called");
        Page<Product> productPage;
        ProductDtoPagedList productDtoPagedList;
        if((designation != null && StringUtils.hasLength(designation))){
            productPage = productRepository.findAllByDesignation(designation,pageRequest);
        }else {
            productPage = productRepository.findAll(pageRequest);
        }
        productDtoPagedList = new ProductDtoPagedList(productPage
                .getContent()
                .stream()
                .map(productMapper::convertEntityToDto)
                .collect(Collectors.toList()), PageRequest.of(productPage.getPageable().getPageNumber(),
                productPage.getPageable().getPageSize()
        ),
                productPage.getTotalElements()
        );


        return productDtoPagedList;
    }

    @Override
    public List<ProductDto> findAll() {
        return productRepository.findAll()
                .stream()
                .map(productMapper::convertEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto save(final ProductDto productDto) {

        // Todo implement validation dto

        return null;
    }

    @Override
    public ProductDto getProductById(final UUID productId) {
        if(productId == null){
            log.debug("Le produit fourni avec l'identifiant ID= "+ productId + " est inexistant");
        }
        return productRepository.getProductById(productId)
                .filter(p -> p.getId().equals(productId))
                .map(productMapper::convertEntityToDto)
                .orElseThrow(
                        () -> new EntityNotFoundException("Le produit fourni avec l'identifiant productId= " + productId + " est inexistant")
                );
    }
}
