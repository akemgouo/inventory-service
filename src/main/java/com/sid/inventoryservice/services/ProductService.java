package com.sid.inventoryservice.services;

import com.sid.inventoryservice.domain.Product;
import com.sid.inventoryservice.web.dto.ProductDto;
import com.sid.inventoryservice.web.dto.ProductDtoPagedList;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface ProductService {

    ProductDtoPagedList listAllProduct(final String designation, PageRequest pageRequest);
    List<ProductDto> findAll();
    ProductDto save(final ProductDto productDto);
    ProductDto getProductById(final UUID productId);
}
