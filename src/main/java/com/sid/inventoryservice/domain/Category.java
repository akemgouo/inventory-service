package com.sid.inventoryservice.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Category implements Serializable {

    @Builder
    public Category(UUID id, Timestamp createDate, String catCode, String designation) {
        Id = id;
        this.createDate = createDate;
        this.catCode = catCode;
        this.designation = designation;
    }

    @Id
    @GeneratedValue(generator = "UUID" )
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(name = "id", length = 36, columnDefinition = "varchar(36)", updatable = false, nullable = false)
    private UUID Id;

    @Version
    private Long version;

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp createDate;

    @UpdateTimestamp
    private Timestamp lastModifiedDate;

    @Column(name = "categorycode", unique = true)
    @GeneratedValue(generator = "cat-generator")
    @GenericGenerator(name = "cat-generator", parameters = @Parameter(name = "prefix", value = "CAT"),
            strategy = "com.sid.inventoryservice.domain.generetor.CodeGenerator")
    private String catCode;

    private String designation;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, cascade  = CascadeType.ALL, orphanRemoval = true)
    private List<Product> products;

}
